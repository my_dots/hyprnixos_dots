{ config, pkgs, unstable, ...}: {

  imports = [
    ./modules/hyprland.nix
    ./modules/alacritty.nix
    ./modules/waybar/waybar.nix
    ./modules/zsh.nix
    ./modules/hyprlock.nix
    ./modules/neofetch/default.nix
    ./modules/swaync/swaync.nix
  ];

  home = {
    username = "wiz";
    homeDirectory = "/home/wiz";
    stateVersion = "24.11";

    pointerCursor = {
      package = pkgs.catppuccin-cursors.latteDark;
      name = "catppuccin-latte-dark-cursors";
      size = 22;
      gtk.enable = true;
      x11.enable = true;
    };

    sessionVariables = {
      XCURSOR_SIZE = config.home.pointerCursor.size;
      XCURSOR_THEME = config.home.pointerCursor.name;
    };



  };

  gtk = {
    enable = true;
    theme = {
      package = pkgs.catppuccin-gtk.override {
        size = "standard";
        accents = ["lavender"];
        variant = "mocha";
      };
    name = "catppuccin-mocha-lavender-standard";
    };

    font = {
      name = "JetBrainsMono Nerd Font";
      size = 12;
    };

    iconTheme ={
      package = pkgs.catppuccin-papirus-folders.override {
        flavor = "mocha";
        accent = "lavender";
      };
      name = "Papirus-Dark";
    };




  };

  xdg.mimeApps = {
    enable = true;
    associations.added = {
      "image/png" = ["swayimg.desktop"];
      "image/jpeg" = ["swayimg.desktop"];
      "x-scheme-handler/http" = ["firefox.desktop"];
      "x-scheme-handler/https" = ["firefox.desktop"];
      "x-scheme-handler/chrome" = ["firefox.desktop"];
      "application/x-extension-htm" = ["firefox.desktop"];
      "application/x-extension-html" = ["firefox.desktop"];
      "application/x-extension-shtml" = ["firefox.desktop"];
      "application/xhtml+xml" = ["firefox.desktop"];
      "application/x-extension-xhtml" = ["firefox.desktop"];
      "application/x-extension-xht" = ["firefox.desktop"];
    };
    defaultApplications = {
      "image/png" = ["swayimg.desktop"];
      "image/jpeg" = ["swayimg.desktop"];
      "x-scheme-handler/http" = ["firefox.desktop"];
      "x-scheme-handler/https" = ["firefox.desktop"];
      "x-scheme-handler/chrome" = ["firefox.desktop"];
      "application/x-extension-htm" = ["firefox.desktop"];
      "application/x-extension-html" = ["firefox.desktop"];
      "application/x-extension-shtml" = ["firefox.desktop"];
      "application/xhtml+xml" = ["firefox.desktop"];
      "application/x-extension-xhtml" = ["firefox.desktop"];
      "application/x-extension-xht" = ["firefox.desktop"];
    };
  };
}
