{ config, ... }: {
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    # enableAutosuggestions = true;
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;

    shellAliases =
      let
        flakeDir = "~/nix-catppuccin";
      in {
      sudo = "doas";
      rb = "doas nixos-rebuild switch --flake ${flakeDir}";
      upd = "doas nix flake update ${flakeDir}";
      upg = "doas nixos-rebuild switch --upgrade --flake ${flakeDir}";

 
      do = "doas";
  
      hms = "home-manager switch --flake ${flakeDir}";

      vi = "nvim";

      };

    history.size = 10000;
    history.path = "${config.xdg.dataHome}/zsh/history";

    oh-my-zsh = {
      enable = true;
      plugins = [ "git" "sudo" ];
      theme = "frisk"; # blinks is also really nice
    };
  };
}
