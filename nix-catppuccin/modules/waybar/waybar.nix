{pkgs, ...}: {
  programs.waybar = {
    enable = true;

    settings = [
      {
        layer = "top";
        position = "top";
        exclusive = true;
        fixed-center = true;
        gtk-layer-shell = true;
        spacing = 0;
        margin-top = 0;
        margin-bottom = 0;
        margin-left = 0;
        margin-right = 0;
        modules-left = [ "custom/nixlogo" "hyprland/workspaces"];
        modules-center = [ "custom/weather" "clock"];
        modules-right = [
          "tray"
          "pulseaudio"
          "hyprland/language"
          "custom/notification"
          "custom/power"
        ];

        "custom/weather" = {
          format = "{}";
          exec = "~/nix-catppuccin/modules/waybar/weather.sh";
          restart-interval = 50;
          on-click = "firefox https://wttr.in/";
        };


        "custom/power" = {
          format = "{icon}";
          format-icons = " ";
          on-click = "~/nix-catppuccin/modules/rofi/powermenu/type-4/powermenu.sh";
          escape = true;
          tooltip = false;
        };

        # Logo
        "custom/nixlogo" = {
          format = "{icon}";
          format-icons = " ";
          tooltip = false;
          on-click = "~/nix-catppuccin/modules/rofi/launchers/type-6/launcher_2.sh";
        };

        # Workspaces
        "hyprland/workspaces" = {
          format = "{name}";
          on-click = "activate";
          disable-scroll = true;
          all-outputs = true;
          show-special = true;
          persistent-workspaces = {"*" = 5;};
        };

        # Clock & Calendar
        clock = {
          format = "{:%a %b %d, %H:%M}";
          on-click = "${pkgs.eww}/bin/eww update showcalendar=true";
          actions = {
            on-scroll-down = "shift_down";
            on-scroll-up = "shift_up";
          };
        };

        # Tray
        tray = {
          icon-size = 20;
          show-passive-items = true;
          spacing = 6;
        };

        "hyprland/language" = {
          format = "{}";
          format-en = "US";
          format-ru = "RU";
        };

        # Notifications
        "custom/notification" = {
          exec = "${pkgs.swaynotificationcenter}/bin/swaync-client -swb";
          return-type = "json";
          format = "{icon}";
          format-icons = {
            notification = "󰂚";
            none = "󰂜";
            dnd-notification = "󰂛";
            dnd-none = "󰪑";
            inhibited-notification = "󰂛";
            inhibited-none = "󰪑";
            dnd-inhibited-notification = "󰂛";
            dnd-inhibited-none = "󰪑";
          };
          on-click = "${pkgs.swaynotificationcenter}/bin/swaync-client -t -sw";
          on-click-right = "${pkgs.swaynotificationcenter}/bin/swaync-client -d -sw";
          escape = true;
        };

        # Pulseaudio
        pulseaudio = {
          format = "{volume} {icon} / {format_source}";
          format-source = "󰍬";
          format-source-muted = "󰍭";
          format-muted = "󰖁 / {format_source}";
          format-icons = {default = ["󰕿" "󰖀" "󰕾"];};
          on-click = "${pkgs.pamixer}/bin/pamixer --toggle-mute";
          on-click-right = "pavucontrol";
          on-scroll-down = "${pkgs.pamixer}/bin/pamixer --decrease 1";
          on-scroll-up = "${pkgs.pamixer}/bin/pamixer --increase 1";
          tooltip = false;
        };
      }
    ];

    style = builtins.readFile (./. + "/style.css");
  };
}
