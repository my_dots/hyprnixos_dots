{
  programs.alacritty = {
    enable = true;
    settings = {
      window.opacity = 0.95;

      font = {
        size = 12.0;
        # draw_bold_text_with_bright_colors = true;
        normal = {
          family = "JetBrains Mono";
          style = "Bold";
        };
      };

      colors.primary.background = "#1e1e2e";
      colors.primary.foreground = "#cdd6f4";

      colors.normal.black = "#45475a";
      colors.normal.red = "#f38ba8";
      colors.normal.green = "#a6e3a1";
      colors.normal.yellow = "#f9e2af";
      colors.normal.blue = "#89b4fa";
      colors.normal.magenta = "#f5c2e7";
      colors.normal.cyan = "#94e2d5";
      colors.normal.white = "#bac2de";

      colors.bright.black = "#585b70";
      colors.bright.red = "#f38ba8";
      colors.bright.green = "#a6e3a1";
      colors.bright.yellow = "#f9e2af";
      colors.bright.blue = "#89b4fa";
      colors.bright.magenta = "#f5c2e7";
      colors.bright.cyan = "#94e2d5";
      colors.bright.white = "#a6adc8";

      #cursor.text = "CellBackground";
      #cursor.cursor = "CellForeground";
    };
  };
}
