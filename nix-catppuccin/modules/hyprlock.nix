{ config, ... }: {
  programs.hyprlock ={
    enable = true;

    settings = {

      general = {};

      background = [ {
        monitor = "HDMI-A-1";
        path = "~/nix-catppuccin/modules/Wallpapers/wallpaper.png";
        blur_passes = "2";
        blur_size = "2";
      } ];

      input-field = [ {
        monitor = "HDMI-A-1";
        size = "190, 30";
        outline_thickness = "2";
        dots_size = "0.33";
        dots_spacing = "0.15";
        dots_center = true;
        outer_color = "rgba(40,40,40,0.0)";
        inner_color = "rgba(200, 200, 200, 0.8)";
        font_color = "rgba(10, 10, 10, 0.8)";
        fade_on_empty = false;
        placeholder_text = "Enter Password";
        hide_input = false;

        position = "0, 100";
        halign = "center";
        valign = "bottom";
      } ];

      label = [ {
        monitor = "HDMI-A-1";
        text = " $USER";
        color = "gba(200, 200, 200, 1.0)";
        font_size = "18";
        font_family = "JetBrainsMono";
        position = "0, 150";
        halign = "center";
        valign = "bottom";
      } ];
    };
  };
}
