{ config, pkgs, ... }: {
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;

    settings = {
      "$mainMod" = "SUPER";

      monitor = "HDMI-A-1,1920x1080@143Hz,0x0,1";

      env =[
        "XCURSOR_SIZE,30"
        #"QT_QPA_PLATFORMTHEME,qt6ct"
        #"XDG_CURRENT_DESKTOP,Hyprland"
        #"XDG_SESSION_TYPE,wayland"
        #"XDG_SESSION_DESKTOP,Hyprland"
        #"QT_AUTO_SCREEN_SCALE_FACTOR,1"
        #"QT_QPA_PLATFORM,wayland"
        "HYPRCURSOR_THEME,Catppuccin-Latte-Dark-Cursors"
        "HYPRCURSOR_SIZE,30"
      ];

      input = {
        kb_layout = "us,ru";
        kb_variant = "lang";
        kb_options = "grp:caps_toggle";

        follow_mouse = 1;

        sensitivity = 0;

        touchpad = {
          natural_scroll = false;
        };



      };

      exec-once = [
        #"/home/wiz/nix-catppuccin/modules/spoof-dpi/bin/spoof-dpi"
        "hyprlock"
        "swaybg -i ~/nix-catppuccin/modules/Wallpapers/008.jpg"
        "udiskie --automount"
        "waybar"
        "telegram-desktop -startintray"
      ];

      general = {
        gaps_in = 5;
        gaps_out = 10;
        border_size = 2;
        "col.active_border" = "rgba(b4befeee) 45deg";
        "col.inactive_border" = "rgba(595959aa)";

        layout = "dwindle";

      };

      decoration = {
        rounding = 5;

        blur = {
          enabled = true;
          size = 3;
          passes = 2;
          new_optimizations = true;
        };

      };

      animations = {
        enabled = true;

        bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";
        #bezier = "myBezier, 0.33, 0.82, 0.9, -0.08";

        animation = [
          "windows,     1, 3,  default"
          "windowsOut,  1, 7,  default, popin 80%"
          "border,      1, 10, default"
          "borderangle, 1, 8,  default"
          "fade,        1, 7,  default"
          "workspaces,  1, 6,  default"
        ];
      };

      dwindle = {
        pseudotile = true; # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
        preserve_split = true; # you probably want this
      };

      gestures = {
        workspace_swipe = true;
        workspace_swipe_fingers = 3;
        workspace_swipe_invert = false;
        workspace_swipe_distance = 200;
        workspace_swipe_forever = true;
      };

      misc = {
        animate_manual_resizes = false;
        animate_mouse_windowdragging = false;
        enable_swallow = false;
        render_ahead_of_time = false;
        disable_hyprland_logo = true;
      };

      windowrule = [
        "float, ^(pavucontrol)$"
        "size 739 400,^(pavucontrol)$"
        "move 1171 55,^(pavucontrol)$"
        "float, ^(mpv)$"
        "size 1120 630,^(mpv)$"
        "size 683 384,^(alacritty)$"
      ];

      bind = [
        "$mainMod, Return, exec, alacritty"
        "$mainMod, S, exec, rofi -show window -theme /home/wiz/nix-catppuccin/modules/rofi/launchers/type-6/style-1.rasi"
        "$mainMod, D, exec, ~/nix-catppuccin/modules/rofi/launchers/type-6/launcher.sh"
        '', F12, exec, grim ~/Pictures/screen-"$(date +%s)".png''
        ''$mainMod, F12, exec, grim -g "$(slurp)" ~/Pictures/screen-"$(date +%s)".png''
        "$mainMod, E, exec, pcmanfm"
        "$mainMod, L, exec, ~/nix-catppuccin/modules/rofi/powermenu/type-4/powermenu.sh"
        ", XF86AudioRaiseVolume, exec, pamixer --increase 2"
        ", XF86AudioLowerVolume, exec, pamixer --decrease 2"

        "$mainMod SHIFT, Q, killactive,"
        "$mainMod, C, exit,"
        "$mainMod, F, togglefloating,"
        "$mainMod SHIFT, F, fullscreen,"
        #"$mainMod CTRL, P, exec, poweroff"
        #"$mainMod CTRL, R, exec, reboot"

        "$mainMod, left,  movefocus, l"
        "$mainMod, right, movefocus, r"
        "$mainMod, up,    movefocus, u"
        "$mainMod, down,  movefocus, d"

        "$mainMod SHIFT, left,  swapwindow, l"
        "$mainMod SHIFT, right, swapwindow, r"
        "$mainMod SHIFT, up,    swapwindow, u"
        "$mainMod SHIFT, down,  swapwindow, d"

        "$mainMod CTRL, left,  resizeactive, -60 0"
        "$mainMod CTRL, right, resizeactive,  60 0"
        "$mainMod CTRL, up,    resizeactive,  0 -60"
        "$mainMod CTRL, down,  resizeactive,  0  60"

        "$mainMod, 1, workspace, 1"
        "$mainMod, 2, workspace, 2"
        "$mainMod, 3, workspace, 3"
        "$mainMod, 4, workspace, 4"
        "$mainMod, 5, workspace, 5"
        "$mainMod, 6, workspace, 6"
        "$mainMod, 7, workspace, 7"
        "$mainMod, 8, workspace, 8"
        "$mainMod, 9, workspace, 9"
        "$mainMod, 0, workspace, 10"

        "$mainMod SHIFT, 1, movetoworkspace, 1"
        "$mainMod SHIFT, 2, movetoworkspace, 2"
        "$mainMod SHIFT, 3, movetoworkspace, 3"
        "$mainMod SHIFT, 4, movetoworkspace, 4"
        "$mainMod SHIFT, 5, movetoworkspace, 5"
        "$mainMod SHIFT, 6, movetoworkspace, 6"
        "$mainMod SHIFT, 7, movetoworkspace, 7"
        "$mainMod SHIFT, 8, movetoworkspace, 8"
        "$mainMod SHIFT, 9, movetoworkspace, 9"
        "$mainMod SHIFT, 0, movetoworkspace, 10"

        "$mainMod, mouse_down, workspace, e+1"
        "$mainMod, mouse_up, workspace, e-1"
      ];

       bindm = [
        "$mainMod, mouse:272, movewindow"
        "$mainMod, mouse:273, resizewindow"
      ];
    };
  };
}
