# HyprNixOS dots

## Catppuccin

<br />
  
<div align="center">
<img src="https://gitlab.com/my_dots/hyprnixos_dots/-/raw/main/.img/cat/desktop.png?ref_type=heads" width="550">
<img src="https://gitlab.com/my_dots/hyprnixos_dots/-/raw/main/.img/cat/nitch.png?ref_type=heads" width="550">
<img src="https://gitlab.com/my_dots/hyprnixos_dots/-/raw/main/.img/cat/rofi.png?ref_type=heads" width="550">
<img src="https://gitlab.com/my_dots/hyprnixos_dots/-/raw/main/.img/cat/hyprlock.png?ref_type=heads" width="550">
</div>
<br /><br />

## Zapret
Вы можете включить zapret в configuration.nix раскоментировав десятую строку
